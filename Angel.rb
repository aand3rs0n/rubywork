def favorite_number name
  if name == 'Tracy'
    return 'Tracy. Your favorite number is 12? cool.'
  end

  if name == 'Robby'
    return 'Robby. Your favorite number is 69? niiiiicccceeeee'
  end

  if name == 'Lacy'
    return "Lacy. Your favorite number is 420? HAH!\nLacy, you're not the pure child everyone thinks you are. Are you. PLOT TWIST!!"
  end
  'You don\'t have a favorite number?? That\'s pretty lame ' + name + '.'
end

puts favorite_number('Robby')
puts favorite_number('Drake')
puts favorite_number('Tracy')
puts favorite_number('Lacy')
puts favorite_number('Josh')
puts favorite_number('Lucy')
