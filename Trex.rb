colors = ['navy', 'magenta', 'scarlet']
puts
puts colors
puts
puts colors.to_s
puts
puts colors.join(', ')
puts
puts colors.join(' :P ')
puts
puts colors.join
puts

4.times do
  puts ['.']
end

4.times do
 puts ''
end

4.times do
  puts []
end

4.times do
  puts
end

puts

favorites = ['I']
puts favorites
favorites.push 'love'
puts favorites.join ' '
favorites.push 'hate'
puts favorites.join ' '
puts favorites[0]
puts favorites.last
puts favorites [0] + ' ' + favorites [1] + ' ' + favorites [0] + ' ' + favorites [2]
puts favorites.length
puts favorites.pop
puts favorites.join ' '
puts favorites.length
puts favorites.pop
puts favorites
puts favorites.length
