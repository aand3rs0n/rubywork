puts 'Do you want to try my survey? It\'s super fun!!'
gets.chomp
puts 'Awesome!! Let\'s get started.'
def ask_recursively question
  puts question
  reply = gets.chomp.downcase

  if reply == 'yes'
    true
  elsif reply == 'no'
    false
    ask_recursively question
  else
    puts 'Please answer "yes" or "no"'
    ask_recursively question
  end
end

ask_recursively 'Are you a stupid stupidhead?'
ask_recursively 'What\'s your favorite color?'
ask_recursively 'Do you love this survey?'
