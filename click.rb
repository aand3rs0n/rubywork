
name = "Lilac"

puts "My name is " + name + "."
#is the same as
puts "My name is #{name}."

if
9<=9
puts "true"
end

10>=5
# ^^works too

puts "
while true
  if at_stop
    stop_train
    break
  else
    drive_train
    break
  end
end
"

while true
  puts "hi"
  break
end
# "break" breaks the while loop

# "if" is just a conditional block vs "while" which is a loop, example:

# just kike if has an unless, while has an until
# ^
puts "
until at_stop
  drive_train
end"
# related to stuff in fork.rb




i = 1
if i == 1
  puts "yay"
end

# vs

while i<10
  puts "woo"
 i += 1
end
