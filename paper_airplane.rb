def recursive question
  puts question
  reply = gets.chomp.downcase
  if reply == 'yes'
    boo = true
    puts boo
  elsif reply == 'no'
    boo = false
    puts boo
    recursive question
  else
    puts 'please answer "yes" or "no".'
    recursive question
  end
end

recursive 'what are tacos?'
recursive 'can you eat them?'
recursive 'but like, do they taste bad?'
recursive 'Oh, okay. Thanks for telling me. What\'s your name again?'
recursive 'your name is "yes"?'

# can use true and false to trigger other objects and shit
# __________________________________________________________________


M = 'land'
o = 'water'
world = [[o,o,o,o,o,o,o,o,o,o,o],
         [o,o,o,o,M,M,o,o,o,o,o],
         [o,o,o,o,o,o,o,o,M,M,o],
         [o,o,o,M,o,o,o,o,o,M,o],
         [o,o,o,M,o,M,M,o,o,o,o],
         [o,o,o,o,M,M,M,M,o,o,o],
         [o,o,o,M,M,M,M,M,M,M,o],
         [o,o,o,M,M,o,M,M,M,o,o],
         [o,o,o,o,o,o,M,M,o,o,o],
         [o,M,o,o,o,M,o,o,o,o,o],
         [o,o,o,o,o,o,o,o,o,o,o]]
def continent_size world, x, y
  if world[y][x] != 'land'
# Either it' s water or we already
# counted it, but either way, we don' t # want to count it now.
return 0
end
  #  So first we count this tile...
size = 1
world[y][x] = 'counted land'
# ...then we count all of the
# neighboring eight tiles (and,
# of course, their neighbors by
# way of the recursion).
size = size + continent_size(world, x-1, y-1)
size = size + continent_size(world, x, y-1)
size = size + continent_size(world, x+1, y-1)
size = size + continent_size(world, x-1, y)
size = size + continent_size(world, x+1, y)
size = size + continent_size(world, x-1, y+1)
size = size + continent_size(world, x, y+1)
size = size + continent_size(world, x+1, y+1)

 size
end
puts continent_size(world, 5, 5)
