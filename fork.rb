
grocery_list = ["cheese", "grapes", "bread"]
for item in grocery_list
   if item.length.odd?
  puts item
end
end
# puts odd- needs 2 ends b/c two conditionals


grocery_list = ["cheese", "grapes", "bread"]
for item in grocery_list
   next if item.length.odd?
  puts item
end
# puts even b/c "next" skips the "if item.legth.odd" if it is true







places = ["Lilly Square", "Candy Kingdom", "", "Vampire Lair", "Tree House", "Ice Castle"]
places.each do |stop|
  next if stop.empty?
  puts 'hi'
end



places = ["Lilly Square", "Candy Kingdom", "Vampire Lair", "Tree House"]
places.each do |stop|
  next if stop.empty?
  puts "boo"
end



# ___________________________________________________________________________
places = ["Lilly Square", "Candy Kingdom", "Vampire Lair", "Tree House"]
places.each do |stop|
  next if stop.empty?
end
# same as. just looks more elegant as


places = ["Lilly Square", "Candy Kingdom", "Vampire Lair", "Tree House"]
places.each {|stop| next if stop.empty?}
